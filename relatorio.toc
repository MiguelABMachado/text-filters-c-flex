\select@language {portuguese}
\contentsline {part}{I\hspace {1em}Introdu\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {part}{II\hspace {1em}Resolu\IeC {\c c}\IeC {\~a}o dos problemas}{4}
\contentsline {section}{\numberline {1}Processamento de entidades nomeadas}{4}
\contentsline {subsection}{\numberline {1.1}Problema}{4}
\contentsline {subsection}{\numberline {1.2}Filtro e an\IeC {\'a}lise}{4}
\contentsline {subsection}{\numberline {1.3}Exemplo}{6}
\contentsline {section}{\numberline {2}Museu da Pessoa \IeC {\textemdash } tratamento de fotografias}{9}
\contentsline {subsection}{\numberline {2.1}Filtro e an\IeC {\'a}lise}{9}
\contentsline {subsection}{\numberline {2.2}Exemplo}{14}
\contentsline {part}{III\hspace {1em}Conclus\IeC {\~a}o}{15}
