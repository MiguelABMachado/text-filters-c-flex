%{
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>


	FILE *f;
	char *nome, *data, *descricao, *indice_pessoas;


	struct indice{
		char *indice_pessoas;
		struct indice *left;
		struct indice *right;
	};


	struct foto{

		char *foto;
		char *data;
		char *descricao;
		struct foto *left;
		struct foto *right;
	};

	

	struct foto* newnode(char* foto, char *data, char *descricao);
	struct foto* insert(struct foto* node, char *foto, char *data, char *descricao);
	void printHtml(struct foto *album);

	struct indice* newnode2(char* indice_pessoas);
	struct indice* insert2(struct indice* node, char *indice_pessoas);
	void printHtml2(struct indice *pessoas);



	struct foto *album;
	struct indice *pessoas;
%}


%%


"<foto ".+		{
				indice_pessoas= "";
				nome = "";			
				data = "NULL";
				descricao = "";

				nome = strtok(yytext,"\"");	
				nome = strtok(NULL,"\""); 	
				}
"<quando ".+	{
					data = strtok(yytext,"\"");	
					data = strtok(NULL,"\"");	


				}
"<quando>".+	{								
					int i;
					data = strtok(yytext,"<");	
					
					for(i=0;i<7;i++) 
						*data++;
				}

"<facto>"([^<]|\n)+"</facto>"	{
					int i;

					descricao = strtok(yytext,"<");
					for(i=0;i<6;i++)
						*descricao++;
					

				}

"<quem>"([^<]|\n)*	{
					
					if (strstr(yytext,",")!=NULL){
						indice_pessoas= strtok(strtok(yytext,","),">");
						indice_pessoas= strdup(strtok(NULL,">"));
					}
					else {
						indice_pessoas= strtok(yytext,">");
						indice_pessoas= strdup(strtok(NULL,">"));
					}
					if (indice_pessoas[0] == ' ') {
						*indice_pessoas++;
					}
					if (indice_pessoas[strlen(indice_pessoas)-1] == ' '){
						indice_pessoas[strlen(indice_pessoas)-1]='\0';
					}
					

					pessoas = insert2(pessoas, indice_pessoas);
					
 
				}

"</foto>"		{

					album = insert(album,nome,data,descricao);

				}

.|\n;

%%


int yywrap()
{
	return(1); 
}

int main()
{
	album = NULL;
	pessoas=NULL;
	f = fopen("teste.html","w");

	fprintf(f,"<html>\n");
	fprintf(f,"<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"pt-br\" xml:lang=\"pt-br\">\n");
  	fprintf(f,"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
	yylex();
	fprintf(f, "<h1>Indice de Pessoas ordenado alfabeticamente</h1>");
	printHtml2(pessoas);
	fprintf(f, "<h1>Fotografias com legenda e ordenadas por data</h1>");	
	printHtml(album);
	fprintf(f,"</html>");
	return 0; 
}



struct foto* newnode(char* foto, char *data, char *descricao){
	
	struct foto* node = (struct foto*)malloc(sizeof(struct foto));
	node->foto = foto;
	node->data = data;
	node->descricao = descricao;
	node->left = NULL;
	node->right = NULL;
	

	return(node);
}


struct foto* insert(struct foto* node, char *foto, char *data, char *descricao){

	if (node == NULL){	
		return(newnode(foto,data,descricao));
	}
 	if (strcmp(data, node->data) == -1){
 		node->left = insert(node->left, foto, data, descricao);
    }
    else{    
      	node->right = insert(node->right, foto, data, descricao);
    }
    


    return node;
}

void printHtml(struct foto *album){

  	if(album != NULL){
  		printHtml(album->left);
  		fprintf(f, "<p><img src=\"%s\" lang=\"pt-pt\" style=\"width:304px;height:228px\">\n",album->foto);
  		fprintf(f, "%s</p>",album->descricao);
		printHtml(album->right);
	}
}



struct indice* newnode2(char* indice_pessoas){
	
	struct indice* node = (struct indice*)malloc(sizeof(struct indice));
	node->indice_pessoas = indice_pessoas;
	node->left = NULL;
	node->right = NULL;
	

	return(node);
}

struct indice* insert2(struct indice* node, char *indice_pessoas){

	if (node == NULL)	
		return(newnode2(indice_pessoas));
	if (strcmp(indice_pessoas, node->indice_pessoas) != 0){ // se for igual nao mete
 		if (strcmp(indice_pessoas, node->indice_pessoas) == -1) 
    		node->left = insert2(node->left, indice_pessoas);
    	else
        	node->right = insert2(node->right, indice_pessoas);
    }
    

    return node;
}


void printHtml2(struct indice *pessoas){

  	if(pessoas != NULL){
  		printHtml2(pessoas->left);
   		fprintf(f, "<p>%s</p>",pessoas->indice_pessoas);
		printHtml2(pessoas->right);
	}
}











