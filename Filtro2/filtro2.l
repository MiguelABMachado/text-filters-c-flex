%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "substring.c"
#include "avl.c"
char* valString;
TAVL pessoas;
TAVL paises;
TAVL cidades;
TAVL organizacoes;
FILE* filename;
void printPessoas(TAVL node,FILE* filename);
void printPaises(TAVL node,FILE* filename);
void printCidades(TAVL node,FILE* filename);
void printOrganizacoes(TAVL node,FILE* filename);
%}

pessoa "<ENAMEX TYPE=\"PERSON\">"[a-zA-Z À-ý]+"</ENAMEX>"
pais "<ENAMEX TYPE=\"LOCATION\" SUBTYPE=\"COUNTRY\">"[a-zA-Z À-ý]+"</ENAMEX>"
cidade "<ENAMEX TYPE=\"LOCATION\" SUBTYPE=\"CITY\">"[a-zA-Z À-ý]+"</ENAMEX>"
organizacao "<ENAMEX TYPE=\"ORGANIZATION\">"[a-zA-Z À-ý]+"</ENAMEX>"

%%
{pessoa} {
		valString=substring(yytext,23,strlen(yytext));
		cutter(valString,strlen(valString),9);
		pessoas=insert(pessoas,valString);
		}
			
{pais} {
		valString=substring(yytext,43,strlen(yytext));
		cutter(valString,strlen(valString),9);
		paises=insert(paises,valString);
	}
		
{cidade} {
		valString=substring(yytext,40,strlen(yytext));
		cutter(valString,strlen(valString),9);
		cidades=insert(cidades,valString);
	}
		
{organizacao} {
		valString=substring(yytext,29,strlen(yytext));
		cutter(valString,strlen(valString),9);
		organizacoes=insert(organizacoes,valString);
	}
		
.|\n 	   {;}

%%
int yywrap()
{ return(1); }

int main(int argc,char** argv){ 
	if(argc==2){
		yylex();
		filename=fopen(argv[1],"w+");
		fprintf(filename,"<html>\n<head>\n<meta charset=\"UTF-8\">\n</head>\n<body>");
		fprintf(filename,"<h1>Pessoas</h1>\n");
		printPessoas(pessoas,filename);
		fprintf(filename,"<h1>Paises</h1>\n");
		printPaises(paises,filename);
		fprintf(filename,"<h1>Cidades</h1>\n");
		printCidades(cidades,filename);
		fprintf(filename,"<h1>Organizações</h1>\n");
		printOrganizacoes(organizacoes,filename);
		fprintf(filename,"</body>\n</html>");
		fclose(filename);
	}else{
		printf("Número errado de argumentos.");
	}
	return 0; 
}

void printPessoas(TAVL node,FILE* filename){
	if(node!=NULL){
		printPessoas(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printPessoas(node->right,filename);
	}
}

void printPaises(TAVL node,FILE* filename){
	if(node!=NULL){
		printPaises(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);;
		printPaises(node->right,filename);
	}
}

void printCidades(TAVL node,FILE* filename){
	if(node!=NULL){
		printCidades(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printCidades(node->right,filename);
	}
}

void printOrganizacoes(TAVL node,FILE* filename){
	if(node!=NULL){
		printOrganizacoes(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printOrganizacoes(node->right,filename);
	}
}