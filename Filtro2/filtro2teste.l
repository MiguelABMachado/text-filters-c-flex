%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avl.c"
TAVL pessoas,paises,cidades,organizacoes;
char *pessoa,*pais,*cidade,*organizacao;
FILE* filename;
void printPessoas(TAVL node,FILE* filename);
void printPaises(TAVL node,FILE* filename);
void printCidades(TAVL node,FILE* filename);
void printOrganizacoes(TAVL node,FILE* filename);
%}

pessoa "<ENAMEX TYPE=\"PERSON\">"[ ]*
pais "<ENAMEX TYPE=\"LOCATION\" SUBTYPE=\"COUNTRY\">"[ ]*
cidade "<ENAMEX TYPE=\"LOCATION\" SUBTYPE=\"CITY\">"[ ]*
organizacao "<ENAMEX TYPE=\"ORGANIZATION\">"[ ]*

%x VEMPESSOA VEMPAIS VEMCIDADE VEMORGANIZACAO
%%


{pessoa} {BEGIN VEMPESSOA;}
{pais} {BEGIN VEMPAIS;}
{cidade} {BEGIN VEMCIDADE;}
{organizacao} {BEGIN VEMORGANIZACAO;}

<VEMPESSOA>"</ENAMEX>" {BEGIN INITIAL;}
<VEMPESSOA>[a-zA-Z À-ý.]+[ ]* {pessoa=strdup(yytext);pessoas=insert(pessoas,pessoa);}
<VEMPAIS>"</ENAMEX>" {BEGIN INITIAL;}
<VEMPAIS>[a-zA-Z À-ý.]+[ ]*  {pais=strdup(yytext);paises=insert(paises,pais);}
<VEMCIDADE>"</ENAMEX>" {BEGIN INITIAL;}
<VEMCIDADE>[a-zA-Z À-ý.]+[ ]* {cidade=strdup(yytext);cidades=insert(cidades,cidade);}
<VEMORGANIZACAO>"</ENAMEX>" {BEGIN INITIAL;}
<VEMORGANIZACAO>[a-zA-Z À-ý.]+[ ]* {organizacao=strdup(yytext);organizacoes=insert(organizacoes,organizacao);}

.|\n 	   {;}

%%
int yywrap()
{ return(1); }

int main(int argc,char** argv){ 
	if(argc==2){
		yylex();
		filename=fopen(argv[1],"w+");
		fprintf(filename,"<html>\n<head>\n<meta charset=\"UTF-8\">\n</head>\n<body>");
		fprintf(filename,"<h1>Pessoas</h1>\n");
		printPessoas(pessoas,filename);
		fprintf(filename,"<h1>Paises</h1>\n");
		printPaises(paises,filename);
		fprintf(filename,"<h1>Cidades</h1>\n");
		printCidades(cidades,filename);
		fprintf(filename,"<h1>Organizações</h1>\n");
		printOrganizacoes(organizacoes,filename);
		fprintf(filename,"</body>\n</html>");
		fclose(filename);
	}else{
		printf("Número errado de argumentos.");
	}
	return 0; 
}

void printPessoas(TAVL node,FILE* filename){
	if(node!=NULL){
		printPessoas(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printPessoas(node->right,filename);
	}
}

void printPaises(TAVL node,FILE* filename){
	if(node!=NULL){
		printPaises(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);;
		printPaises(node->right,filename);
	}
}

void printCidades(TAVL node,FILE* filename){
	if(node!=NULL){
		printCidades(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printCidades(node->right,filename);
	}
}

void printOrganizacoes(TAVL node,FILE* filename){
	if(node!=NULL){
		printOrganizacoes(node->left,filename);
		fprintf(filename,"<p>%s</p>\n",node->id);
		printOrganizacoes(node->right,filename);
	}
}