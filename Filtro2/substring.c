#include <stdio.h>
#include <stdlib.h>
 
char* substring(char*, int, int);
  
/*C substring function: It returns a pointer to the substring */
 
char *substring(char *string, int position, int length) 
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(1);
   }
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *(string+position-1);      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}

/*It removes the last N elements from a char* */

void cutter(char *string,int length,int n){
   *(string+length-n) = '\0';
}