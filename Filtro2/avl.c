#include <stdlib.h>
#include <string.h>
#include "avl.h"

typedef struct MAVL{ // AVL DE idS
	char* id; 
	struct MAVL *left;
	struct MAVL *right;
	int height; //Height height
}MAVL;

/*AVL* init() {
  AVL* n = (MAVL*) malloc (sizeof(MAVL));
  n->id=NULL;
  n->left = NULL;
  n->right = NULL;
  n->height=1;
  return n;
}*/

int height(TAVL N){
	if (N == NULL) return 0;
	return N-> height;
}


int max(int a, int b){
	return (a > b) ? a : b;
}

TAVL newNode (char * id){
	TAVL node = (TAVL)malloc(sizeof(MAVL));
	node->id=id;
	node->left=NULL;
	node->right=NULL;
	node->height=1;
	return node;
}

TAVL rightRotate(TAVL y){

    TAVL x = y->left;
    TAVL z = x->right;
 
    x->right = y;
    y->left = z;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 
TAVL leftRotate(TAVL x){

    TAVL y = x->right;
    TAVL T2 = y->left;
 
    y->left = x;
    x->right = T2;
 
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;

    return y;
}

int getBalance(TAVL N){ // retorna o balanceamento
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}


int procura(TAVL node, char* id){

	if(node == NULL) return 0;
	if (strcmp(id,node->id) == 0) return 1;
    else if (strcmp(id,node->id) < 0) return procura(node->left, id);
    else return procura(node->right, id);
	
}

TAVL insert(TAVL node, char* id){
	int balance = 0;
	if (node == NULL)
        return(newNode(id));
	if (strcmp(id,node->id) != 0){
		if (strcmp(id,node->id) < 0) node->left  = insert(node->left, id);
        else node->right = insert(node->right, id);
    	node->height = max(height(node->left), height(node->right)) + 1;
		balance = getBalance(node);
		if (balance > 1 && strcmp(id,node->left->id) < 0) return rightRotate(node);
		if (balance < -1 &&  strcmp(id,node->right->id) >= 0) return leftRotate(node);
        if (balance > 1 &&  strcmp(id,node->left->id) >= 0){
            node->left =  leftRotate(node->left);
            return rightRotate(node);
        }
        if (balance < -1 &&  strcmp(id,node->right->id) < 0){
            node->right = rightRotate(node->right);
            return leftRotate(node);
        }
     	return node;
    }
    else {return node;}
 }
 




