#ifndef AVL
#define AVL



typedef struct MAVL* TAVL;


/*AVL* init();*/
int height(TAVL N);
int max(int a, int b);
TAVL newNode (char * id);
TAVL rightRotate(TAVL y);
TAVL leftRotate(TAVL x);
int getBalance(TAVL N);
int procura(TAVL node, char* id);
TAVL insert(TAVL node, char* id);

#endif